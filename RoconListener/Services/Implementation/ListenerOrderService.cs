﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using RoconListener.Models;
using RoconLibrary.Models;
using RoconLibrary.Utility;
using RoconLibrary.Services;
using RoconLibrary.Services.Implementation;
using RoconListener.Utility;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RoconLibrary.Domains;
using AutoMapper;
using Serilog;

namespace RoconListener.Services.Implementation
{
    public class ListenerOrderService : IListenerOrderService
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        private readonly HttpClient httpClient = new HttpClient();

        public ListenerOrderService(IConfigurationRoot config, IMapper mapper)
        {
            _config = config;
            _mapper = mapper;

            var kafkaTimeout = Int32.Parse(config["kafkaApiTimeoutInSeconds"]);
            Log.Information($"Kafka api timeout: {kafkaTimeout} seconds");
            httpClient.Timeout = TimeSpan.FromSeconds(kafkaTimeout);
            string envTokenKey = "KAFKA_AUTH";
            string accessToken = Environment.GetEnvironmentVariable(envTokenKey);
            if (accessToken == null)
            {
                throw new Exception($"No value was found for environment variable key [{envTokenKey}]");
            }

            // Console.WriteLine($"\n=========>Access token is: {accessToken} \n");
            httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + accessToken);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public void processStart(OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            var orderStatusConfigs = getOrderStatusConfigs(connection);
            Log.Debug("==> Number of status configurations: " + orderStatusConfigs.Count);
             
            var oraclReader = getOrderStatusReader(connection);

            while (oraclReader.Read())
            {
                OrderStatusEntity orderStatusEntity = null;
                try
                {
                    orderStatusEntity = getOrderStatusEntity(oraclReader);
                    Log.Debug("OrderStatus ==> " + orderStatusEntity.OrderNumber + " " + orderStatusEntity.Status + " " + orderStatusEntity.StatusDtTm);

                    var publishOrderStatuses = getOrderPublishStatus(orderStatusEntity, connection);

                    Order order = getRoconOrder(orderStatusEntity, orderStatusConfigs, publishOrderStatuses, connection);
                    var success = sendToKafka(order, connection);

                    if (success.Result)
                    {
                        Log.Information("*** Calling updateDatabase lco_order_status");
                        SQLHelper.updateDatabase(orderStatusEntity, connection);
                    }
                }
                catch (Exception e)
                {
                    string orderNumber = "";
                    if(orderStatusEntity != null)
                    {
                        orderNumber = orderStatusEntity.OrderNumber;
                    }

                    var errorInfo = $"===> For Order Id: [{orderNumber}] a general error exception happened. Message = [{e.Message}. Stacktrace is: {e.StackTrace}]";
                    Log.Error(errorInfo);
                    SQLHelper.logToErrorService(errorInfo, connection);

                }
            }
            oraclReader.Close();
        }

        private OracleDataReader getOrderStatusReader(OracleConnection connection)
        {
            var oraclCmd = connection.CreateCommand();
            oraclCmd.CommandText = "SELECT CO_ODNO, STATUS, STATUSSEQUENCE, STATUSCYCLEPATH, STATUS_DT_TM, EXTRACT_DT_TM"
            + " FROM LCO_ORDER_STATUS"
            + " WHERE EXTRACT_DT_TM IS null AND STATUSCYCLEPATH IN"
            + " (SELECT DISTINCT STATUSCYCLEPATH FROM LCO_ORDER_STATUS_CONFIG) ORDER BY LOAD_DT_TM ASC, STATUSSEQUENCE ASC";

            return oraclCmd.ExecuteReader();
        }

        private List<OrderStatusDetail> getOrderStatusConfigs(OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            //Log.Debug("Inside getOrderStatusConfig");
            List<OrderStatusDetail> orderStatusConfigs = new List<OrderStatusDetail>();
            var oraclCmd = connection.CreateCommand();
            oraclCmd.CommandText = "SELECT STATUSCYCLEPATH, STATUSSEQUENCE, STATUSIDENTIFIER, STATUSDESC, CYCLEDESC FROM LCO_ORDER_STATUS_CONFIG";
            var oraclReader = oraclCmd.ExecuteReader();
            while (oraclReader.Read())
            {
                orderStatusConfigs.Add(new OrderStatusDetail
                {
                    statusPathName = ((string)oraclReader["CYCLEDESC"]).Trim(),
                    statusSequenceNo = Int32.Parse((string)oraclReader["STATUSSEQUENCE"]),
                    statusIdentifier = ((string)oraclReader["STATUSIDENTIFIER"]).Trim(),
                });
            }
            oraclReader.Close();
            return orderStatusConfigs;
        }

        private OrderStatusEntity getOrderStatusEntity(OracleDataReader oraclReader)
        {
            if (oraclReader["STATUS_DT_TM"] is DBNull)
            {
                throw new Exception("STATUS_DT_TM is NULL in table LCO_ORDER_STATUS FOR order number: " + ((string)oraclReader["CO_ODNO"]).Trim());
            }

            var orderStatusEntity = new OrderStatusEntity
            {
                OrderNumber = ((string)oraclReader["CO_ODNO"]).Trim(),
                Status = ((string)oraclReader["STATUS"]).Trim(),
                StatusSequence = ((string)oraclReader["STATUSSEQUENCE"]).Trim(),
                StatusCyclePath = ((string)oraclReader["STATUSCYCLEPATH"]).Trim(),
                StatusDtTm = (DateTime)oraclReader["STATUS_DT_TM"]
            };
            return orderStatusEntity;
        }

        private List<OrderStatusDetail> getOrderPublishStatus(OrderStatusEntity orderStatusEntity, OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            var oraclCmd = new OracleCommand("GET_ORDER_PUBLISHED_STATUS", connection);
            oraclCmd.CommandType = CommandType.StoredProcedure;

            oraclCmd.Parameters.Add("LV_SORORDERNUMBER", OracleDbType.Varchar2).Value = orderStatusEntity.OrderNumber;
            oraclCmd.Parameters.Add("REF_ORDERPUBLISHEDSTATUS", OracleDbType.RefCursor, ParameterDirection.InputOutput);

            var oraclReader = oraclCmd.ExecuteReader();

            List<OrderStatusDetail> publishOrderStatuses = new List<OrderStatusDetail>();
            while (oraclReader.Read())
            {
                publishOrderStatuses.Add(new OrderStatusDetail
                {
                    statusSequenceNo = (Int32.Parse(((string)oraclReader["statusSequenceNo"]).Trim())),
                    statusIdentifier = ((string)oraclReader["statusIdentifier"]).Trim(),
                    statusCreateTime = FormatUtils.AddTimezone(((DateTime)oraclReader["statusCreateTime"]).ToString())
                });
            }
            oraclReader.Close();
            return publishOrderStatuses;
        }

        private Order getRoconOrder(OrderStatusEntity orderStatusEntity,
                                           List<OrderStatusDetail> orderStatusConfigs,
                                           List<OrderStatusDetail> publishOrderStatuses,
                                           OracleConnection connection)
        {
            Task<Order> taskOrder = SQLHelper.GetOrderByIdAsync(orderStatusEntity,
                                                                 connection,
                                                                 _mapper);
            var order = taskOrder.Result;
            string systemIdentifier = "R";
            string systemName = "R:ROCON";
            order.orderHeader.orderSourceInfo = new OrderSourceInfo();
            order.orderHeader.orderSourceInfo.systemIdentifier = systemIdentifier;
            order.orderHeader.orderSourceInfo.systemName = systemName;
            order.orderHeader.orderSourceInfo.systemOrderStatusDetail = orderStatusConfigs.ToArray();
            order.orderHeader.publishedOrderStatus = removePublishedOrderStatus(order, publishOrderStatuses).ToArray();
            order.orderHeader.lcoOrderSystem = new SystemIdentify();
            order.orderHeader.lcoOrderSystem.systemIdentifier = systemIdentifier;
            order.orderHeader.lcoOrderSystem.systemName = systemName;
            order.orderHeader.sorSystem = new SystemIdentify();
            order.orderHeader.sorSystem.systemIdentifier = systemIdentifier;
            order.orderHeader.sorSystem.systemName = systemName;

            return order;
        }

        private List<OrderStatusDetail> removePublishedOrderStatus(Order order,
                                                                    List<OrderStatusDetail> publishOrderStatuses)
        {
            var statusCode = order.orderHeader.orderStatus.orderStatusCode;
            var orderStatusDetails = new List<OrderStatusDetail>();
            foreach (OrderStatusDetail orderStatusDetail in publishOrderStatuses)
            {
                orderStatusDetails.Add(orderStatusDetail);
                // Log.Information($"=====> Status code: [{statusCode}] and status identifier [{orderStatusDetail.statusIdentifier}]]");
                if (statusCode.Equals(orderStatusDetail.statusIdentifier))
                {
                    // Log.Information("==========> Breaking");
                    break;
                }
            }
            return orderStatusDetails;
        }

        private async Task<bool> sendToKafka(Order order, OracleConnection connection)
        {
            bool success = true;
            var payload = JsonConvert.SerializeObject(order);
            // Log.Debug("Sending this payload to kafka api: \n" + payload);

            try
            {
                var href = _config["kafkaApiHref"];
                Log.Information($"The kafka api href is:[ {href}]");

                var httpResponseMessage = await httpClient.PostAsync(new Uri(href),
                    new StringContent(payload, Encoding.UTF8, "application/json"));

                var statusCode = (int)httpResponseMessage.StatusCode;
                string resp = await httpResponseMessage.Content.ReadAsStringAsync();

                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    Log.Information($"The was a Kafka success status code [{statusCode.ToString()}] for order id: [{order.orderHeader.sorOrderNumber}]");
                }
                else
                {
                    Log.Error($"The was a Kafka failure status code [{statusCode.ToString()}] for order id: [{order.orderHeader.sorOrderNumber}]");
                    success = false;
                }
                SQLHelper.insertToLcoOrderJson(order, payload, resp, statusCode.ToString(), "", connection);
            }
            catch (Exception e)
            {
                var errorInfo = $"Caught exception from Kafka api : [{e.Message}]. Stacktracke [{e.StackTrace}]";
                Log.Error(errorInfo);
                SQLHelper.logToErrorService(errorInfo, connection);
                success = false;
            }
            Log.Information($"Returning from Kakfa for order {order.orderHeader.lcoOrderNumber} result: {success}");
            return success;
        }
    }
}
