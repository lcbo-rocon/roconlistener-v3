#!/usr/bin/env bash

# This will generate the packaging local file that
# can be used by other projects ...

dotnet nuget locals all --clear

dotnet restore

# make sure to change the version if you want
# in RoconLibray.csproj
dotnet pack -c Release -o PackagingFile