﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;


namespace RoconLibrary.Domains
{
    public static class OrderJsonValidator
    {

        public static IList<string> ValidateOrderJson(string schemaFile, OrderForm order)
        {
            JSchema schema = JSchema.Parse(File.ReadAllText(schemaFile));
            JToken json = JToken.Parse(JsonConvert.SerializeObject(order));

            IList<string> errors;

            json.IsValid(schema, out errors);

            return errors;

        }

        //public static bool ValidateProductJson(string schemaFile, Product product)
        //{

        //}

        //public static bool ValidateCustomerJson(string schemaFile, Customer customer)
        //{

        //}
}

        

        
    
    //internal class OrderEventConverter : JsonConverter
    //{
    //    public override bool CanConvert(Type t) => t == typeof(OrderEvent) || t == typeof(OrderEvent?);

    //    public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
    //    {
    //        if (reader.TokenType == JsonToken.Null) return null;
    //        var value = serializer.Deserialize<string>(reader);
    //        switch (value)
    //        {
    //            case "CANCEL":
    //                return OrderEvent.Cancel;
    //            case "NEW":
    //                return OrderEvent.New;
    //            case "RETURN":
    //                return OrderEvent.Return;
    //        }
    //        return OrderEvent.Exception;
    //       // throw new Exception(value + " is not a valid OrderEvent! Valid values: {NEW,CANCEL,RETURN}");
    //    }

    //    public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
    //    {
    //        if (untypedValue == null)
    //        {
    //            serializer.Serialize(writer, null);
    //            return;
    //        }
    //        var value = (OrderEvent)untypedValue;
    //        switch (value)
    //        {
    //            case OrderEvent.Cancel:
    //                serializer.Serialize(writer, "CANCEL");
    //                return;
    //            case OrderEvent.New:
    //                serializer.Serialize(writer, "NEW");
    //                return;
    //            case OrderEvent.Return:
    //                serializer.Serialize(writer, "RETURN");
    //                return;
    //            case OrderEvent.Exception:
    //                serializer.Serialize(writer, "INVALID!");
    //                return;
    //        }
    //        throw new Exception("Cannot marshal type OrderEvent " + value + " is not valid!");
            
    //    }

    //    public static readonly OrderEventConverter Singleton = new OrderEventConverter();
    //}
    //internal class IndicatorConverter : JsonConverter
    //{
    //    public override bool CanConvert(Type t) => t == typeof(Indicator) || t == typeof(Indicator?);

    //    public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
    //    {
    //        if (reader.TokenType == JsonToken.Null) return null;
    //        var value = serializer.Deserialize<string>(reader);
    //        switch (value)
    //        {
    //            case "N":
    //                return Indicator.N;
    //            case "Y":
    //                return Indicator.Y;
    //        }
    //        throw new Exception("Cannot unmarshal type Indicator");
    //    }

    //    public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
    //    {
    //        if (untypedValue == null)
    //        {
    //            serializer.Serialize(writer, null);
    //            return;
    //        }
    //        var value = (Indicator)untypedValue;
    //        switch (value)
    //        {
    //            case Indicator.N:
    //                serializer.Serialize(writer, "N");
    //                return;
    //            case Indicator.Y:
    //                serializer.Serialize(writer, "Y");
    //                return;
    //        }
    //        throw new Exception("Cannot marshal type Indicator");
    //    }

    //    public static readonly IndicatorConverter Singleton = new IndicatorConverter();
    //}
    //internal class DeliveryTypeConverter : JsonConverter
    //{
    //    public override bool CanConvert(Type t) => t == typeof(DeliveryType) || t == typeof(DeliveryType?);

    //    public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
    //    {
    //        if (reader.TokenType == JsonToken.Null) return null;
    //        var value = serializer.Deserialize<string>(reader);
    //        switch (value)
    //        {
    //            case "PICK_UP":
    //                return DeliveryType.PICK_UP;
    //            case "SHIP_TO_ADDR_ON_FILE":
    //                return DeliveryType.SHIP_TO_ADDR_ON_FILE;
    //        }
    //        throw new Exception("Cannot unmarshal type DeliveryType");
    //    }

    //    public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
    //    {
    //        if (untypedValue == null)
    //        {
    //            serializer.Serialize(writer, null);
    //            return;
    //        }
    //        var value = (DeliveryType)untypedValue;
    //        switch (value)
    //        {
    //            case DeliveryType.PICK_UP:
    //                serializer.Serialize(writer, "PICK_UP");
    //                return;
    //            case DeliveryType.SHIP_TO_ADDR_ON_FILE:
    //                serializer.Serialize(writer, "SHIP_TO_ADDR_ON_FILE");
    //                return;
    //        }
    //        throw new Exception("Cannot marshal type DeliveryType");
    //    }

    //    public static readonly DeliveryTypeConverter Singleton = new DeliveryTypeConverter();
    //}
}

