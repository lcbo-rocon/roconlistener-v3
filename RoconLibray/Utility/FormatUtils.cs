﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconLibrary.Utility
{
    public static class FormatUtils
    {
        public static string  ConvertToYYYYMMDD(string dt)
        {
            DateTime yyyymmdd = new DateTime();

            DateTime.TryParse(dt, out yyyymmdd);

            return yyyymmdd.ToString("yyyy-MM-dd");

        }

        public static string ConvertToYYYYMMDDHHMISS(string dt)
        {
            DateTime yyyymmdd = new DateTime();

            DateTime.TryParse(dt, out yyyymmdd);

            return yyyymmdd.ToString("yyyy-MM-dd HH:mm:ss");

        }

        public static DateTime ToDate(string dt)
        {
            DateTime newDate = new DateTime();
            DateTime.TryParse(dt, out newDate);

            return newDate;
        }

        public  static string AddTimezone(string strDate)
        {
            string dateWithTimeZone = strDate;
            var dt = new DateTime();


            if (DateTime.TryParse(strDate, out dt))
            {
                // dateWithTimeZone = dt.ToString("yyyy-MM-ddThh:mm:ss") + "-0500";
                var dtz = new DateTimeOffset(dt);
                dateWithTimeZone = dtz.ToString("yyyy-MM-ddTHH:mm:sszzz");

            }

            return dateWithTimeZone;
        }

        public static IEnumerable<List<T>> splitList<T>(List<T> locations, int nSize = 30)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }

        public static string getOrderStatusName(string status)
        {
            string statusName = null;
            if(string.Equals(status, "W"))
            {
                statusName = "W:WAIT";
            }
            else if (string.Equals(status, "O"))
            {
                statusName = "O:OPEN";
            }
            else if (string.Equals(status, "R"))
            {
                statusName = "R:READY FOR PICK";
            }
            else if (string.Equals(status, "P"))
            {
                statusName = "P:PICK COMPLETED";
            }
            else if (string.Equals(status, "I"))
            {
                statusName = "I:INVOICED";
            }
            else if (string.Equals(status, "C"))
            {
                statusName = "C:CLOSED";
            }
            else if (string.Equals(status, "D"))
            {
                statusName = "D:DELETED_CANCELLED";
            }
            else if (string.Equals(status, "E"))
            {
                statusName = "E:ERROR";
            }
            else if (string.Equals(status, "Z"))
            {
                statusName = "Z:ZERO PICKED";
            }
            else
            {
                throw new Exception($"The passed status: [{status}] is not valid");
            }

            return statusName;
        }
    }
}
