﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace RoconLibrary.Utility
{
    public static class Authorization
    {
        public static bool isAuthorized(string bToken)
        {
            bool soup = false; //no soup for you
            var check = Environment.GetEnvironmentVariable("CHECKSOUP");
            var secret = Environment.GetEnvironmentVariable("SNAKEWITHLEGS");
            
            if (check == "1")
            {
                //Remove text "Bearer "
                var compare = Regex.Replace(bToken, "^Bearer\\s+", "");
                soup = (compare == secret);
            }
            else
            {
                //soup for everyone
                soup = true;
            }

            

            return soup;
        }
    }
}
