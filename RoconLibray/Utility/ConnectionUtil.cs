﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using Serilog;

namespace RoconLibrary.Utility
{
    public static class ConnectionUtil
    {
        public static void openConnection(OracleConnection connection)
        {
            if (connection.State == System.Data.ConnectionState.Closed)
            {
                Log.Information("Open connection...");
                connection.Open();
                Log.Information("Connected to DB server: " + connection.ServerVersion);
            }
        }

        public static OracleConnection getConnection(IConfiguration config)
        {
            var oraConn = config["connectionstr"];
            return new OracleConnection(oraConn);
        }
    }
}
