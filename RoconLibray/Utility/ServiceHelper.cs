﻿using AutoMapper;
using Dapper.Oracle;
using Dapper;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RoconLibrary.Utility
{
    class ServiceHelper
    {
        ///<summary>
        /// Need to save Jason string
        /// Need to insert O status to orderstatus history
        /// Silver 2.0, change Order status to be O:Open when OMS send order request.
        /// </summary>
        public static Task<Order> CreateOrder(Order order,
                                    IEnumerable<ProductInventory> productList,
                                    IMapper mapper,
                                    IDbConnection conn)
        {
            //build orderDetails table
            foreach (var curitem in order.orderItems)
            {
                var ProdInv = productList.Where(p => p.sku == curitem.sku).First();
                curitem.priceInfo = mapper.Map<OrderItemPriceInfo>(ProdInv.priceInfo);
                curitem.item_category = ProdInv.item_category;
            }

            //build orderHeader invoice totals
            setInvoiceTotals(order);

            //ok now we start writing to the tables
            //Need to save jason string
            //Need to check if order been cancelled
            //Need to insert orderstatus history table with O
            conn.Open();
            int nextInvNo = getNextInvoiceNo(conn);
            bool isSuccess = (nextInvNo != -1);

            if (isSuccess)
            {
                commitNewOrder(order, nextInvNo, conn);
            }
            return Task.FromResult(order);
        }

        private static void setInvoiceTotals(Order order)
        {
            if (order.orderHeader.invoiceInfo == null)
            {
                order.orderHeader.invoiceInfo = new InvoiceInfo();
            }

            order.orderHeader.invoiceInfo.InvoiceTotalAmount = order.orderItems.Sum(p => p.priceInfo.selling_price * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceDiscountAmount = order.orderItems.Sum(p => p.priceInfo.discount * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceBottleDepositAmount = order.orderItems.Sum(p => p.priceInfo.bottle_deposit * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceHstAmount = order.orderItems.Sum(p => p.priceInfo.hst_tax * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceLicMuAmount = order.orderItems.Sum(p => p.priceInfo.liquor_markup * p.quantity);
            order.orderHeader.invoiceInfo.InvoiceLevyAmount = 0m;
            order.orderHeader.invoiceInfo.InvoiceRetailAmount = order.orderItems.Sum(p => p.priceInfo.retail_price * p.quantity);


            if (order.orderHeader.invoiceInfo.DeliveryCharge == null)
            {
                order.orderHeader.invoiceInfo.DeliveryCharge = new DeliveryCharge();
                order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryBaseAmount = 0m;
                order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryHstAmount = 0m;

                //TODO: When we implement the proper delivery charge structure this logic should be removed.
                if (order.orderHeader.deliveryType == DeliveryType.SHIP_TO_ADDR_ON_FILE.ToString())
                {
                    order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryChargeType = DeliveryChargeType.CalculateDelivery.ToString();
                }
                else
                {
                    order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryChargeType = DeliveryChargeType.PickUp.ToString();
                    if (order.orderHeader.orderCommentsInfo == null)
                    {
                        order.orderHeader.orderCommentsInfo = new OrderCommentsInfo()
                        {
                            BolComment = "PICK UP ORDER"
                        };
                    }
                }
            }
        }

        private static int getNextInvoiceNo(IDbConnection conn)
        {
            int nextInvNo = -1;

            using (var tran = conn.BeginTransaction())
            {
                var param = new OracleDynamicParameters();
                param.Add("lv_nextInvNo", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_NEXT_INVOICE_NO";

                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    nextInvNo = Decimal.ToInt32(param.Get<Decimal>("lv_nextInvNo"));
                    //No rolling back as this can impact other users getting an inv_no from ROCON
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    var sErrorList = new List<ErrorDetail>();

                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "INVALID_ORDER",
                        errorMessage = "ERROR Generating Invoice Number",
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.OTHER.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                }
                finally
                {
                    //report error
                }
            }

            return nextInvNo;
        }

        private static void commitNewOrder(Order order, int nextInvNo, IDbConnection conn)
        {
            using (var tran = conn.BeginTransaction())
            {
                string storeProc = "LCBO.COMMIT_NEW_ORDER_V2";

                var parmlist = new OracleDynamicParameters();
                parmlist.Add("lv_invNumber", nextInvNo, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                parmlist.Add("lv_orderNumber", order.orderHeader.sorOrderNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                parmlist.Add("lv_OrderType", order.orderHeader.orderType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                parmlist.Add("lv_ReturnCode", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);
                parmlist.Add("lv_startStage", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                parmlist.Add("lv_invoiceAmount", order.orderHeader.invoiceInfo.InvoiceTotalAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                parmlist.Add("lv_deliveryCharge", order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryBaseAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                parmlist.Add("lv_deliveryTax1", 0m, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                parmlist.Add("lv_deliveryTax2", order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryHstAmount, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                parmlist.Add("lv_fullCases", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                parmlist.Add("lv_partCases", 0, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);

                //--Silver V2.0 new fields
                parmlist.Add("lv_orderSource", order.orderHeader.orderSourceInfo.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                parmlist.Add("lv_orderSourceName", order.orderHeader.orderSourceInfo.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                parmlist.Add("lv_orderStatusCode", order.orderHeader.orderStatus.orderStatusCode, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                parmlist.Add("lv_orderStatusName", order.orderHeader.orderStatus.orderStatusName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                parmlist.Add("lv_status_DT_TM", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.orderStatus.statusCreateTime), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                //--lco and sor ref should not change during order's life cycle. Info were saved to LCO_ORDER_REF table during 'W' status.
                //parmlist.Add("lv_lcoSysIdentifier", order.orderHeader.lcoOrderSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                //parmlist.Add("lv_lcoSysName", order.orderHeader.lcoOrderSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                //parmlist.Add("lv_sorSysIdentifier", order.orderHeader.sorSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                //parmlist.Add("lv_sorSysName", order.orderHeader.sorSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                var refInfos = order.orderHeader.referenceInfo.ToArray();
                int refNum = 1;
                string refParm = "lv_ref";
                foreach (var refInf in refInfos)
                {
                    parmlist.Add(refParm + refNum.ToString() + "_SysID", refInf.referenceSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    parmlist.Add(refParm + refNum.ToString() + "_SysName", refInf.referenceSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                    //order# in diff system
                    parmlist.Add(refParm + refNum.ToString() + "_Identifier", refInf.referenceIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add(refParm + refNum.ToString() + "_IdentifierType", refInf.referenceIdentifierType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add(refParm + refNum.ToString() + "_IdentifierSubtype", refInf.referenceIdentifierSubtype, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    parmlist.Add(refParm + refNum.ToString() + "_Comment", refInf.referenceComment, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    refNum = refNum + 1;
                }

                bool isSuccess = false;
                try
                {
                    conn.Execute(storeProc, param: parmlist, commandType: CommandType.StoredProcedure);

                    var returnCode = Decimal.ToInt32(parmlist.Get<Decimal>("lv_ReturnCode"));

                    if (returnCode == 0)
                    {
                        tran.Commit();
                        isSuccess = true;
                    }
                    else
                    {
                        var sb = new System.Text.StringBuilder();
                        string errTable = "";
                        switch (returnCode)
                        {
                            case 1:
                                errTable = "DBO.ON_INVOICE";
                                break;
                            case 2:
                                errTable = "DBO.ON_INV_CO";
                                break;
                            case 3:
                                errTable = "DBO.ON_INV_CODTL";
                                break;
                            case 4:
                                errTable = "DBO.ON_INV_CODTL_RULE";
                                break;
                            case 6:
                                errTable = "DBO.ON_COHDR (ORDER IN CANCELLED STATUS)";
                                break;
                            default:
                                errTable = "Unknown";
                                break;
                        }
                        sb.AppendLine("Store Proc error: LCBO.COMMIT_NEW_ORDER");
                        sb.AppendLine("Return Code:" + returnCode);
                        sb.AppendLine("ERROR writing to " + errTable);
                        throw new Exception(sb.ToString());
                    }
                }
                catch (Exception ex)
                {
                    var sErrorList = new List<ErrorDetail>();

                    sErrorList.Add(new ErrorDetail()
                    {
                        errorType = "INVALID_ORDER",
                        errorMessage = "Order " + order.orderHeader.lcoOrderNumber + " status cannot be changed to OPEN!",
                        errorData = new ErrorDetail.DataValue()
                        {
                            key = ErrorType.OTHER.ToString(),
                            value = ex.Message.ToString()
                        }
                    });
                    order.errorInfo = sErrorList.ToArray();
                }
                finally
                {
                    if (!isSuccess) tran.Rollback();
                }
            }
        }
    }
}
