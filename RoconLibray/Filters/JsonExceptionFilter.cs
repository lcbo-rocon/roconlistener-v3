﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using RoconLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconLibrary.Filters
{
    public class JsonExceptionFilter : IExceptionFilter 
    {
        private readonly IHostingEnvironment _env;

        public JsonExceptionFilter(IHostingEnvironment env)
        {
            _env = env;
        }
        public void OnException(ExceptionContext context)
        {
            var error = new ErrorDetail();
            

            if (_env.IsDevelopment())
            {
                error.errorType = ErrorType.INVALID_ORDER.ToString();
                error.errorMessage = context.Exception.Message;
                //error.errorData.key = context.Exception.StackTrace;
            }
            else
            {
                error.errorMessage = "A server error occurred";
                error.errorData.key = context.Exception.Message;
            }


            context.Result = new ObjectResult(error)
            {
                StatusCode = 500
            };
        }
    }
}
