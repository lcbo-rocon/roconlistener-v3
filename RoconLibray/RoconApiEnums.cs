﻿
namespace RoconLibrary
{
    
    /// Delivery types accepted by ROCON for Agency/LCO
    public enum DeliveryType { PICK_UP, SHIP_TO_ADDR_ON_FILE };

    
    /// Order Event to trigger order creation and update in Rocon.
    /// "NEW", "RETURN", "CANCEL", "INV_HOLD", "STATUS_UPDATE", "OTHER" 
    public enum OrderEvent { Cancel, New, Return, Inv_hold, Status_Update, Other};

    
    /// Identify the source system of the order capture - W=webstore, O=woocommerce, C=eCommerce WCS, M=OMS, R=ROCON
    public enum OrderSource { W, O, C, M, R };

    
    ///  Identity LCO Order Status to be populated by ROCON in response.
    ///  W=Wait, O=Open, R=Ready for Pick, P=Pick completed, I=Invoiced, C=Closed, D=Deleted/Cancelled, E=Error, Z=Zero Pick completed
    public enum OrderStatus { W, O, R, P, I, C, D, E, Z };

    
    /// Payment methods accepted by ROCON for Agency/LCO
    public enum PaymentMethod { CashOnDelivery, Check };

    
    /// to be populated by ROCON in response
    public enum Indicator { N, Y };

    ///<summary>
    /// Delivery Charge Type in ROCON
    public enum DeliveryChargeType { CalculateDelivery, FreeDelivery, PickUp};
 
   
    /// to capture different error messages for ordered Items
    
    /// "INVALID_ORDER", "PRICE_ERROR", "INVENTORY_ERROR", "SKU_ERROR", "MISSING_ELEMENT", "WRONG_FORMAT", "UNRECOGNIZED_ELEMENT", "FILE_CREATE_ERROR", "FILE_DROP_ERROR", "OTHER"]
    public enum OrderItemErrorType {
        INVALID_ORDER, PRICE_ERROR=100, INVENTORY_ERROR, SKU_ERROR,
        MISSING_ELEMENT, WRONG_FORMAT, UNRECOGNIZED_ELEMENT,
        FILE_CREATE_ERROR, FILE_DROP_ERROR, OTHER, INVALID_DATE }
   
    public enum ErrorType { INVALID_ORDER=100, INVALID_VALUE, PRICE_ERROR, INVENTORY_ERROR, SKU_ERROR, MISSING_ELEMENT,
                                WRONG_FORMAT, UNRECOGNIZED_ELEMENT, FILE_CREATE_ERROR, FILE_DROP_ERROR, OTHER, INVALID_DATE
    };


    public enum OrderType { AGY, LIC, REG, LCO}

    //CONSIDERATION TO ADD NEW CUSTOMER TYPE = LCO TO BE SPECIFIC TO LCO PROGRAM
    public enum CustomerType { AGENCY, LICENSEE, REGULAR}

    public enum ReferenceIdentifierType { ORDER_NUMBER, REFERENCE_NUMBER, OTHER}
    public enum ReferenceIdentifierSubtype { LCO_ORDER, DELIVERY_CHARGE, INVOICE_NUMBER, OTHER }

}
