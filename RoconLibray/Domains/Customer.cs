﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace RoconLibrary.Domains
{
    [DataContract]
    [Serializable]
    public class Customer: Resource
    {
        [DataMember(Name = "customerNumber")]
        [JsonProperty("customerNumber")]
        public string customerNumber { get; set; }

        [DataMember(Name = "customerName")]
        [JsonProperty("customerName")]
        public string customerName { get; set; }

        [DataMember(Name = "customerType")]
        [JsonProperty("customerType")]
        public string customerType { get; set; }

        [DataMember(Name = "customerStatus")]
        [JsonProperty("customerStatus")]
        public string customerStatus { get; set; }

        [DataMember]
        [JsonProperty("routeInfo")]
        public virtual RouteInfo routeInfo { get; set; }

        [DataMember]
        [JsonProperty("customerContact")]
        public virtual CustomerContact customerContact { get; set; }

        [DataMember]
        [JsonProperty("shipToInfo")]
        public virtual Address shipToInfo { get; set; }

        [DataMember]
        [JsonProperty("billToInfo")]
        [JsonIgnore]
        public virtual Address billToInfo { get; set; }

        [DataMember]
        [JsonProperty("availableShippingDates")]
       //[JsonIgnore]
        public virtual AvailableShippingDate[] availableShippingDates { get; set; }

    }
 
    public class CustomerContact
    {
        [DataMember(Name = "contactName")]
        [JsonProperty("contactName")]
        public string contactName { get; set; }
        [DataMember(Name = "contactEmail")]
        [JsonProperty("contactEmail")]
        public string contactEmail { get; set; }
    }

    public partial class RouteInfo
    {
        [DataMember(Name = "routeCode")]
        public string routeCode { get; set; }
        [DataMember(Name = "routeStops")]
        public int routeStops { get; set; }
    }

    public class AvailableShippingDate
    {
 
        [DataMember(Name = "ROUTE_CODE")]
        //[JsonProperty("ROUTE_CODE")]
        [JsonIgnore]
        public string ROUTE_CODE { get; set; }
        [DataMember(Name = "ROWNUM")]
        //[JsonProperty("ROWNUM")]
        [JsonIgnore]
        public string ROWNUM { get; set; }

        [DataMember(Name = "cutOffTime")]
        [JsonProperty("cutOffTime")]
        public string cutOffTime { get; set; }
        [DataMember(Name = "orderRequiredDate")]
        [JsonProperty("orderRequiredDate")]
        public string orderRequiredDate { get; set; }
    }
}
