﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace RoconLibrary.Domains
{
    [Serializable]
    [DataContract]
    public class Product  : Resource
    {

        [DataMember(Name = "itemSku")]
        [JsonProperty("itemSku")]
        public int sku { get; set; }

        [JsonProperty("itemName")]
        [DataMember(Name = "itemName")]
        public string description { get; set; }

        [JsonProperty("categoryId")]
        [DataMember(Name ="categoryId")]
        public string item_category { get; set; }

        [JsonProperty("unitOfMeasure")]
        [DataMember(Name = "unitOfMeasure")]
        public string unit_of_measure { get; set; }

        [JsonProperty("unitsPerCase")]
        [DataMember(Name = "unitsPerCase")]
        public int units_per_case { get; set; }

        [JsonProperty("sellingIncrements")]
        [DataMember(Name = "sellingIncrements")]
        public int selling_increments { get; set; }

        [JsonProperty("MaxQty",NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "MaxQty")]
        public int max_qty { get; set; }

        [JsonProperty("itemSize")]
        [DataMember(Name = "itemSize")]
        public int volume { get; set; }

        [JsonProperty("priceInfo")]
        public virtual PriceInfo priceInfo { get; set; }
      
    }

    [Serializable]
    //[JsonProperty(DefaultValueHandling)]
    [DataContract]
    public class PriceInfo
    {
        [DataMember(Name = "custType")]
        [JsonIgnore]
        public string custType { get; set; }

        [DataMember(Name = "itemSellingPrice")]
        [JsonProperty("itemSellingPrice")]
        public decimal selling_price { get; set; }

        [DataMember(Name = "itemBasicPrice")]
        [JsonProperty("itemBasicPrice")]
        public decimal unit_price { get; set; }

        [JsonProperty("itemBottleDeposit")]
        [DataMember(Name = "itemBottleDeposit")]
        public decimal bottle_deposit { get; set; }

        [DataMember(Name = "itemDiscountAmount")]
        [JsonProperty("itemDiscountAmount", NullValueHandling = NullValueHandling.Ignore)]
        public virtual decimal discount { get; set; }      

        [JsonProperty("itemLicMarkupAmount", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "itemLicMarkupAmount")]
        public virtual decimal liquor_markup { get; set; }

        [JsonProperty("itemHSTAmount")]
        [DataMember(Name = "itemHSTAmount")]
        public decimal hst_tax { get; set; }

        [JsonProperty("itemRetailPrice")]
        [DataMember(Name = "itemRetailPrice")]
        public decimal retail_price { get; set; }

        [JsonIgnore]
        [DataMember(Name = "status")]
        public string status { get; set; }

        //[JsonIgnore]
        [JsonProperty("lastUpdateDate", NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "lastUpdateDate")]
        public string last_update { get; set; }


    }

}
