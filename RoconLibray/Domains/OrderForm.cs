﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RoconLibrary.Domains
{
    public class OrderForm: Resource
    {
        [Required]
        public OrderHeader orderHeader { get; set; }

        [Required]
        public OrderItemDetail[] orderItems { get; set; }

        //[JsonProperty("errorDetails", NullValueHandling = NullValueHandling.Ignore)]
        //public ErrorDetail[] errorInfo { get; set; }
    }


}
