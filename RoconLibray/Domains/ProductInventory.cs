﻿using Newtonsoft.Json;

namespace RoconLibrary.Domains
{
    public class ProductInventory: Product
    {

        [JsonProperty("inventory",Order = 1,  NullValueHandling = NullValueHandling.Ignore)]
         public InventoryInfo Inventory { get; set; }
    }

    public partial class InventoryInfo
    {
        [JsonProperty("onHandQty")]
        public int OnHandQuantity { get; set; }

        [JsonProperty("availableForSale")]
        public int AvailableForSale { get; set; }

        [JsonProperty("onOrderQuantity")]
        public int OnOrderQuantity { get; set; }

        [JsonProperty("inTransitQuantity")]
        public int inTransitQuantity { get; set; }
    }
}
