﻿using Newtonsoft.Json;

namespace RoconLibrary.Domains
{
    public partial class Address
    {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string name { get; set; }

        [JsonProperty("addressLine1")]
        public string addressLine1 { get; set; }

        [JsonProperty("addressLine2", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string addressLine2 { get; set; }

        [JsonProperty("city", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string city { get; set; }

        [JsonProperty("provinceCode", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string provinceCode { get; set; }

        [JsonProperty("country", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string country { get; set; }

        [JsonProperty("postalCode", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string postalCode { get; set; }

        [JsonProperty("phoneNumber", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string phoneNumber { get; set; }

        [JsonProperty("faxNumber", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string faxNumber { get; set; }
    }
}
