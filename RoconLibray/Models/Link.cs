﻿using Newtonsoft.Json;
using System.ComponentModel;

namespace RoconLibrary.Domains
{
    public class Link
    {
        public const string GetMethod = "GET";
        public static Link To(string routeName, object routeValues = null)
            => new Link
            {
                RouteName = routeName,
                RouteValues = routeValues,
                Method = GetMethod,
                Relations = null
            };

        public static Link ToCollection(string routeName, object routeValues = null)
            => new Link
            {
                RouteName = routeName,
                RouteValues = routeValues,
                Method = GetMethod,
                Relations = new[] { "collection" }
            };

        [JsonProperty(Order = -4,  NullValueHandling = NullValueHandling.Ignore)]
       // [JsonIgnore]
        public string Href { get; set; }

        //[JsonProperty(Order = -3,
        //    PropertyName = "rel",
        //    NullValueHandling = NullValueHandling.Ignore)]
        [JsonIgnore]
        public string[] Relations { get; set; }

        //[JsonProperty(Order = -1,
        //    DefaultValueHandling = DefaultValueHandling.Ignore,
        //    NullValueHandling = NullValueHandling.Ignore)]
        [JsonIgnore]
        [DefaultValue(GetMethod)]
        public string Method { get; set; }


        //Properties that will be used to generate the route names for the links
        //Stores the route name before being rewritten by the LinkRewritingFilter
        [JsonIgnore]
        public string RouteName { get; set; }

        //Stores the route Values before being rewritten by the LinkRewritingFilter
        [JsonIgnore]
        public object RouteValues { get; set; }
    }

}
