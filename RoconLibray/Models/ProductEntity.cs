﻿using System.Runtime.Serialization;

namespace RoconLibrary.Models
{
    [DataContract]
    public partial class ProductEntity
    {
        //[JsonIgnore]
        //public Guid Id { get; set; }

        public int sku { get; set; }

        public string description { get; set; }

        public string item_category { get; set; }

        public string unit_of_measure { get; set; }

        public int units_per_case { get; set; }

        public int selling_increments { get; set; }

        public int max_qty { get; set; }

        public int volume { get; set; }
        public PriceInfoEntity priceInfo { get; set; }

    }

    public partial class PriceInfoEntity
    {
        public string custType { get; set; }
 
        public decimal selling_price { get; set; }

        public decimal unit_price { get; set; }

        public decimal bottle_deposit { get; set; }

        public decimal discount { get; set; }

        public decimal liquor_markup { get; set; }
        
        public decimal hst_tax { get; set; }

        public decimal retail_price { get; set; }

        public string status { get; set; }

        public string last_update { get; set; }
        
    }
  
}
