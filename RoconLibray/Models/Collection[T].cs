﻿
namespace RoconLibrary.Domains
{
    public class Collection<T> : Resource
    {
        public T[] Value { get; set; }
    }
    public class CustomerList<T>: Resource
    {
        public T[] Customers { get; set; }
    }

    public class ProductList<T> : Resource
    {
        public T[] Products { get; set; }
    }

    public class OrderList<T> : Resource
    {
        public T[] Orders { get; set; }
    }

    public class OrderItemList<T>: Resource
    {
        public T[] OrderItemDetail { get; set; }
    }

}
