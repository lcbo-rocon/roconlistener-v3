﻿
using Newtonsoft.Json;

namespace RoconLibrary.Models
{
    public class ErrorDetail
    {
        [JsonProperty("errorType", Order = 1, NullValueHandling = NullValueHandling.Ignore)]
        public string errorType { get; set; }

        [JsonProperty("errorMessage", Order = 2, NullValueHandling = NullValueHandling.Ignore)]
        public string errorMessage { get; set; }

        [JsonProperty("errorData",Order = 3, NullValueHandling =NullValueHandling.Ignore)]
        public DataValue errorData { get; set; }

        public class DataValue
        { public string key { get; set; }
          public string value { get; set; }
        }
    }
}
