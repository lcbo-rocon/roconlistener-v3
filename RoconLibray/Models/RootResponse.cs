﻿
namespace RoconLibrary.Domains
{
    public class RootResponse : Resource
    {
    
       // public Link Items { get; set; }
        public Link Products { get; set; }
        public Link Customers { get; set; }
        // public Link Info { get; set; }
        public Link Orders { get; set; }
        //  public Link Prices { get; set; }
    }
}
