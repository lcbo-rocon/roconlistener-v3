﻿using System.Runtime.Serialization;

namespace RoconLibrary.Models
{
    [DataContract]
    public  class CustomerEntity
    {
        [DataMember(Name = "customerNumber")]
        public string customerNumber { get; set; }
        [DataMember(Name = "customerName")]
        public string customerName { get; set; }

        [DataMember(Name = "customerType")]
        public string customerType { get; set; }

        [DataMember(Name = "customerStatus")]
        public string customerStatus { get; set; }

        public virtual RouteInfoEntity routeInfo { get; set; }

        public virtual CustomerContactEntity customerContact { get; set; }

        public virtual ShipToInfoEntity shipToInfo { get; set; }

        public virtual BillToInfoEntity billToInfo { get; set; }

        public virtual AvailableShippingDateEntity[] availableShippingDates { get; set; }

    }

    public partial class RouteInfoEntity
    {
        public string routeCode { get; set; }
        public int routeStops { get; set; }
    }

    public class CustomerContactEntity
    {
        [DataMember(Name = "contactName")]
        public string contactName { get; set; }
        [DataMember(Name = "contactEmail")]
        public string contactEmail { get; set; }
    }

    public class ShipToInfoEntity
    {
        [DataMember(Name = "name")]
        public string shipToName { get; set; }
        [DataMember(Name = "addressLine1")]
        public string shipToAddressLine1 { get; set; }
        [DataMember(Name = "addressLine2")]
        public string shipToAddressLine2 { get; set; }
        [DataMember(Name = "city")]
        public string shipToCity { get; set; }
        [DataMember(Name = "provinceCode")]
        public string shipToProvinceCode { get; set; }
        [DataMember(Name = "country")]
        public string shipToCountry { get; set; }
        [DataMember(Name = "postalCode")]
        public string shipToPostalCode { get; set; }
        [DataMember(Name = "phoneNumber")]
        public string shipToPhoneNumber { get; set; }
    }

    public class BillToInfoEntity
    {
        [DataMember(Name = "billToName")]
        public string billToName { get; set; }
        [DataMember(Name = "billToAddressLine1")]
        public string billToAddressLine1 { get; set; }
        [DataMember(Name = "billToAddressLine2")]
        public string billToAddressLine2 { get; set; }
        [DataMember(Name = "billToCity")]
        public string billToCity { get; set; }
        [DataMember(Name = "billToProvinceCode")]
        public string billToProvinceCode { get; set; }
        [DataMember(Name = "billToCountry")]
        public string billToCountry { get; set; }
        [DataMember(Name = "billToPostalCode")]
        public string billToPostalCode { get; set; }
        [DataMember(Name = "billToPhoneNumber")]
        public string billToPhoneNumber { get; set; }
    }

    public class AvailableShippingDateEntity
    {
       public string ROUTE_CODE { get; set; }
       public string ROWNUM { get; set; }
       public string CUTOFF_DT { get; set; }
       public string DELIVERY_DT { get; set; }
    }
   
}

