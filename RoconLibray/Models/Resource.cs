﻿using Newtonsoft.Json;
using RoconLibrary.Models;

namespace RoconLibrary.Domains
{
    public abstract class Resource : Link
    {
        [JsonIgnore]
        public Link Self { get; set; }

        [JsonProperty("errorInfo", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        public virtual ErrorDetail[] errorInfo {get; set;}
    }
}