﻿
namespace RoconLibrary.Models
{
    public partial class AddressEntity
    {
        public string name { get; set; }

        public string addressLine1 { get; set; }
        
        public string addressLine2 { get; set; }
      
        public string city { get; set; }
       
        public string provinceCode { get; set; }
     
        public string country { get; set; }
      
        public string postalCode { get; set; }
      
        public string phoneNumber { get; set; }

        public string faxNumber { get; set; }
    }
}
