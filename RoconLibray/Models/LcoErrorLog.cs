
namespace RoconLibrary.Models
{
    public class LcoErrorLog
    {
        public int logId { get; set; }
        public string lcoService { get; set; }
        public string lcoModule { get; set; }
        public string errorInfo { get; set; }
        public string createdBy { get; set; }
        public string createdDtTM { get; set; }
    }
}