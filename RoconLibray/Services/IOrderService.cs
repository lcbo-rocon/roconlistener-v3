﻿using RoconLibrary.Domains;
using RoconLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoconLibrary.Services
{
    public interface IOrderService
    {
       // Task<IEnumerable<Order>> GetOrders(string orderType);
        Task<Order> GetOrderByIdAsync(string orderID);

        Task<IEnumerable<OrderItemsEntity>> GetOrderItemDetails(string orderID);

        Task<Order> CreateOrder(Order order, IEnumerable<ProductInventory> productList);

        Task<Order> ReserveInventory(Order order, IEnumerable<ProductInventory> productList);

        Task<IEnumerable<OrderStatusDetailEntity>> GetOrderPublishedStatus(string orderID);
    }
}
