﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using Dapper.Oracle;
using RoconLibrary.Domains;
using RoconLibrary.Models;
using RoconLibrary.Services;
using Microsoft.EntityFrameworkCore.Internal;
using System.Text.RegularExpressions;

namespace RoconLibrary.Services.Implementation
{
    public class ProductService : IProductService
    {

        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public ProductService(
                 IConfiguration configuration, //used to get the connectionstring from appsettings
                 IMapper mapper
            )
        {

            _configuration = configuration;
            _mapper = mapper;
        }

        //public async Task<Product> GetProductByIdAsync(string custType, int sku)
        //{

        //    using (IDbConnection conn = Connection)
        //    {
        //        string customerFilter = custType;

        //        var param = new OracleDynamicParameters();
        //        param.Add("ref_sku", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);
        //        param.Add("lv_cust", custType);
        //        param.Add("lv_sku", sku.ToString());

        //        string sQuery = "LCBO.TEST_SP";

        //        if (conn.State != ConnectionState.Open)
        //        {
        //            conn.Open();
        //        }

        //        if (conn.State == ConnectionState.Open)
        //        {

        //            var results = await conn.QueryAsync<ProductEntity, PriceInfoEntity, ProductEntity>(sQuery, map: (prod, price) =>
        //            {
        //                prod.priceInfo = price;
        //                return prod;
        //            }
        //            , splitOn: "unit_price", param: param, commandType: CommandType.StoredProcedure);

        //            return _mapper.Map<Product>(results.SingleOrDefault());
        //        }

        //    }

        //    return null; //otherwise return null
        //}

        public async Task<IEnumerable<Product>> GetProducts(string custType, string skuList)
        {
            using (IDbConnection conn = Connection)
            {
                string customerFilter = custType;
                string skuListFilter = skuList;
                string storeProc = "LCBO.GET_ALLITEMS_BY_CUST";

                var param = new OracleDynamicParameters();
                param.Add("ref_sku", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);
                param.Add("lv_cust", custType);
                if (!String.IsNullOrEmpty(skuListFilter))
                {
                    storeProc = "LCBO.GET_ITEMLIST_BY_CUST";
                    param.Add("lv_sku", skuList);
                }

                string sQuery = storeProc;

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {

                    var results = await conn.QueryAsync<ProductEntity, PriceInfoEntity, ProductEntity>(sQuery, map: (prod, price) =>
                    {
                        prod.priceInfo = price;
                        return prod;
                    }
                    , splitOn: "custType", param: param, commandType: CommandType.StoredProcedure);

                    //Need to map back to Product

                    //var dt = new DateTime();
                    //DateTime.TryParse("2019-06-01 18:30:45", out dt);
                    //var fo = dt.ToString("yyyy-MM-dd hh24:mm:ss A Z");
                    ////dt.t

                    return _mapper.Map<IEnumerable<Product>>(results.ToArray());

                };

            }

            return null; //otherwise return null
        }

        public async Task<IEnumerable<ProductInventory>> GetProductInventory(string custType, string itemList)
        {
            using (IDbConnection conn = Connection)
            {
                string customerFilter = custType;
                string skuListFilter = itemList;

                string storeProc = "LCBO.GET_ITEMLIST_BY_CUST";
                var param = new OracleDynamicParameters();
                param.Add("lv_cust", custType);
                param.Add("lv_sku", itemList);
                param.Add("ref_sku", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);
                
                //if (!String.IsNullOrEmpty(skuListFilter))
                //{
                //    storeProc = "LCBO.GET_ITEMLIST_BY_CUST";
                //    param.Add("lv_sku", itemList);
                //}

                //string sQuery = storeProc;

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {

                    var results = await conn.QueryAsync<ProductInventoryEntity,PriceInfoEntity,ProductInventoryEntity>(storeProc, map: (prod, price) =>
                    {
                        prod.priceInfo = price;
                       // prod.Inventory = inventory;
                        return prod;
                    }
                    , splitOn: "custType", param: param, commandType: CommandType.StoredProcedure);


                    //Need to map back to Product
                    storeProc = "GET_ITEM_INVENTORY_INFO";

                    var paramInv = new OracleDynamicParameters();
                    paramInv.Add("lv_sku", itemList);
                    paramInv.Add("ref_inventory", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                    var prodInv = await conn.QueryAsync<InventoryInfoEntity>(storeProc, param: paramInv, commandType: CommandType.StoredProcedure);

                    // var ProductDTO = _mapper.Map<IEnumerable<ProductInventory>>(results.ToArray());

                    //How to join these two together
                    foreach (var prod in results)
                    {
                        prod.inventory = prodInv.Where(p => p.sku == prod.sku).First();
                    }


                    //var dt = new DateTime();
                    //DateTime.TryParse("2019-06-01 18:30:45", out dt);
                    //var fo = dt.ToString("yyyy-MM-dd hh24:mm:ss A Z");
                    ////dt.t

                    return _mapper.Map<IEnumerable<ProductInventory>>(results.ToArray());

                };

            }

            return null; //otherwise return null
        }

        public IDbConnection Connection
        {
            get
            {
                return new OracleConnection(_configuration.GetConnectionString("roconconnection"));
            }
        }

    }
}
