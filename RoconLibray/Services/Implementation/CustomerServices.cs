﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Dapper.Oracle;
using Dapper;
using AutoMapper;
using System.Collections.Generic;
using System;
using RoconLibrary.Domains;
using RoconLibrary.Utility;
using RoconLibrary.Models;


namespace RoconLibrary.Services.Implementation
{
    public class CustomerServices: ICustomerServices
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public CustomerServices ( IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }
        public IDbConnection Connection
        {
            get
            {
                return new OracleConnection(_configuration.GetConnectionString("roconconnection"));
            }
        }

        public async Task<IEnumerable<AvailableShippingDateEntity>> GetAvailableShippingDatesAsync(string routeCode)
        {
            using (IDbConnection conn = Connection)
            {
                var param = new OracleDynamicParameters();
                param.Add("lv_order_date", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
                param.Add("lv_number_of_available", 5);
                param.Add("lv_route_code", routeCode);
                param.Add("ref_delivery", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_DELIVERY_DT_BY_ROUTE";

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<AvailableShippingDateEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    return results.ToArray();
                }
            }
            return null;
        }

        public async Task<Customer> GetCustomerByNumberAsync(string custNum)
        {
            using (IDbConnection conn = Connection)
            {  
                //need validation if routecode = 9999 or not returning delivery date, customer probably not setup as LCO yet.
                var param = new OracleDynamicParameters();
                param.Add("lv_customer", custNum);
                param.Add("ref_customer", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_CUSTOMER_INFO";

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<CustomerEntity, RouteInfoEntity, CustomerContactEntity,
                        ShipToInfoEntity, BillToInfoEntity,CustomerEntity>(storeProc, map: (cust, route, custc, ship,bill) =>
                        {
                            cust.routeInfo = route;
                            cust.customerContact = custc;
                            cust.shipToInfo = ship;
                            cust.billToInfo = bill;
                            return cust;
                        }, splitOn: "routeCode, contactName, shipToName, billToName", param: param, commandType: CommandType.StoredProcedure);

                    var customerDTO = results.SingleOrDefault();

                    
                    //customer is not found
                    if (customerDTO == null)
                    {
                        return null;
                    }

                    if (customerDTO.customerStatus == "ACTIVE")
                    {
                        var availDTO = await GetAvailableShippingDatesAsync(customerDTO.routeInfo.routeCode);

                        customerDTO.availableShippingDates = availDTO.ToArray();
                    }

                    var customer = _mapper.Map<Customer>(customerDTO);

                    return _mapper.Map<Customer>(customerDTO);
                    
                    
                }
            }
                return null;
        }


        public async Task<IEnumerable<Customer>> GetAllCustomersAsync(string custType)
        {
            using (IDbConnection conn = Connection)
            {
                string custNum = custType;
                var param = new OracleDynamicParameters();
                param.Add("lv_customer", custNum);
                param.Add("ref_customer", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string storeProc = "LCBO.GET_CUSTOMER_INFO";

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<CustomerEntity, RouteInfoEntity, CustomerContactEntity,
                       ShipToInfoEntity, BillToInfoEntity,  CustomerEntity>
                       (storeProc, map: (cust, route, custc, ship, bill) =>
                       {
                           cust.routeInfo = route;
                           cust.customerContact = custc;
                           cust.shipToInfo = ship;
                           cust.billToInfo = bill;
                           //cust.availableShippingDates = shipd;
                           return cust;
                       }, splitOn: "routeCode, contactName, shipToName, billToName", param: param, commandType: CommandType.StoredProcedure);

                    return _mapper.Map<IEnumerable<Customer>>(results.ToArray());
                }
            }
            return null;
        }


    }
}