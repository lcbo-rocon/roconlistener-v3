
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Models;
using System;
using System.Data;
using Serilog;

namespace RoconLibrary.Services.Implementation
{
    public class LcoErrorLogService : ILcoErrorLogService
    {
        private readonly OracleConnection _connection;

        public LcoErrorLogService(OracleConnection connection)
        {
            _connection = connection;
        }

        public void insertToTable(LcoErrorLog lcoErrorLog)
        {
            using (var tran = _connection.BeginTransaction())
            {
                try 
                {
                    var dateStr = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    var errorInfo = lcoErrorLog.errorInfo;
                    if(errorInfo.Length >= 2000)
                    {
                        errorInfo = errorInfo.Substring(0, 2000);
                    }

                    var sql = "Insert into lcbo.LCO_SERVICE_ERR_LOG (LCO_SERVICE, LCO_MODULE, ERROR_INFO, CREATED_BY, CREATED_DT_TM)"
                    + $" values ('{lcoErrorLog.lcoService}', '{lcoErrorLog.lcoModule}', :ERROR_INFO, '{lcoErrorLog.createdBy}', TO_DATE('{dateStr}', 'YYYY-MM-DD HH24:MI:SS'))";
                    
                    Log.Debug($"Insert sql is:\n {sql}");
                    var command = _connection.CreateCommand();
                    command.CommandText = sql;
                    command.Parameters.Clear();
                    command.Parameters.Add(":ERROR_INFO", errorInfo);
                    int inserted = command.ExecuteNonQuery();
                    tran.Commit();
                    Log.Debug("Quantity inserted: " + inserted);
                }
                catch(Exception e)
                {
                    tran.Rollback();
                    Log.Error($"Caught exception inserting to LCO_SERVICE_ERR_LOG: [{e.Message}]. Stacktracke [{e.StackTrace}]");
                }
            }
        }
    }
}