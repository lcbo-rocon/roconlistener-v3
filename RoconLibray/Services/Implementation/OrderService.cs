﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;
using RoconLibrary.Domains;
using RoconLibrary.Utility;
using Microsoft.Extensions.Configuration;
using Dapper.Oracle;
using Dapper;
using AutoMapper;
using System.Collections.Generic;
using System;
using RoconLibrary.Models;

namespace RoconLibrary.Services.Implementation
{
    public class OrderService : IOrderService
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public OrderService(IConfiguration configuration, IMapper mapper)
        {
            _configuration = configuration;
            _mapper = mapper;
        }
        public IDbConnection Connection
        {
            get
            {
                return new OracleConnection(_configuration.GetConnectionString("roconconnection"));
            }
        }

        // Need to save Jason string
        // Silver 2.0, reserve inventory when WCS send order request
        public Task<Order> ReserveInventory(Order order, IEnumerable<ProductInventory> productList)
        {
            // using (var tran = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            IDbConnection conn = Connection;
            conn.Open();

            //set up price info
            foreach (var curitem in order.orderItems)
            {
                var ProdInv = productList.Where(p => p.sku == curitem.sku).First();
                curitem.priceInfo = _mapper.Map<OrderItemPriceInfo>(ProdInv.priceInfo);
                curitem.item_category = ProdInv.item_category;
            }

            bool isSuccess = false;
            using (var tran = conn.BeginTransaction()) //Or however you get the connection
            {
                //Insert into dbo.LCO_Orders for new order
                string storeProc = "LCBO.RESERVE_LCO_ORDER_HEADER_V2";

                var param = new OracleDynamicParameters();
                param.Add("lv_lcoOrderNumber", order.orderHeader.lcoOrderNumber, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                param.Add("lv_orderRequestedDate", FormatUtils.ConvertToYYYYMMDDHHMISS(order.orderHeader.orderRequestDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_orderSource", order.orderHeader.orderSourceInfo.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                param.Add("lv_orderSeourceName", order.orderHeader.orderSourceInfo.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_overwriteShipToInformation", order.orderHeader.shipToInfo.overwriteShipToInformation, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                param.Add("lv_overwriteShipToAddress1", order.orderHeader.shipToInfo.addressLine1, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToAddress2", order.orderHeader.shipToInfo.addressLine2, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToCity", order.orderHeader.shipToInfo.city, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToProvinceCode", order.orderHeader.shipToInfo.provinceCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToPhoneNumber", order.orderHeader.shipToInfo.phoneNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToName", order.orderHeader.shipToInfo.name, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToCountry", order.orderHeader.shipToInfo.country, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_overwriteShipToPostalCode", order.orderHeader.shipToInfo.postalCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_DeliveryType", order.orderHeader.deliveryType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_routeCode", order.orderHeader.routeInfo.routeCode, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_routeStop", order.orderHeader.routeInfo.routeStops, dbType: OracleMappingType.Int32, direction: ParameterDirection.Input);
                //param.Add("lv_order_priority", DBNull.Value); // order.orderHeader.orderPriority);
                param.Add("lv_customerNumber", order.orderHeader.customerNumber, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                param.Add("lv_orderRequiredDate", FormatUtils.ConvertToYYYYMMDD(order.orderHeader.orderRequiredDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_orderShipmentDate", FormatUtils.ConvertToYYYYMMDD(order.orderHeader.orderShipmentDate), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_orderType", order.orderHeader.orderType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: false);
                param.Add("lv_ShiptoName", order.orderHeader.shipToInfo.name, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input, isNullable: true);
                param.Add("lv_em_no", "LCOWOO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                //Silver V2.0 fields
                //param.Add("lv_orderStatus", "W", dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                param.Add("lv_lcoSysIdentifier", order.orderHeader.lcoOrderSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                param.Add("lv_lcoSysName", order.orderHeader.lcoOrderSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                //param.Add("lv_sorSysIdentifier", order.orderHeader.sorSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                //param.Add("lv_sorSysName", order.orderHeader.sorSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
             
                var refInfos = order.orderHeader.referenceInfo.ToArray();
                int refNum = 1;
                string refParm = "lv_ref";
                foreach (var refInf in refInfos)
                {
                    refParm = refParm + refNum.ToString();
                    param.Add(refParm + "_SysID", refInf.referenceSystem.systemIdentifier, dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                    param.Add(refParm + "_SysName", refInf.referenceSystem.systemName, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);

                    //order# in diff system
                    param.Add(refParm + "_Identifier", refInf.referenceIdentifier, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    param.Add(refParm + "_IdentifierType", refInf.referenceIdentifierType, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    param.Add(refParm + "_IdentifierSubtype", refInf.referenceIdentifierSubtype, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    param.Add(refParm + "_Comment", refInf.referenceComment, dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                    refNum = refNum + 1;
                }

                param.Add("lv_sorOrderNumber", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                try
                {
                    conn.Execute(storeProc, param: param, commandType: CommandType.StoredProcedure);

                    var newOrderNumber = param.Get<Decimal>("lv_sorOrderNumber");

                    order.orderHeader.sorOrderNumber = Decimal.ToInt32(newOrderNumber);

                    if (order.orderHeader.sorSystem == null)
                    {
                        order.orderHeader.sorSystem = new SystemIdentify();
                        order.orderHeader.sorSystem.systemIdentifier = "R";
                        order.orderHeader.sorSystem.systemName = "R:ROCON";
                    }

                    //why discount, licMarkup, ItemRetailPrice still showing with 0 value ??
                    //why numberoffullcase, numberofpartialcass still showing with 0 value??
                    //why showing shippedqty??
                    //why errorInfo still showing with []??
                    //where to define&check response json??

                    order.orderHeader.orderSourceInfo.systemIdentifier = "R";
                    order.orderHeader.orderSourceInfo.systemName = "R:ROCON";

                    var sortedOrderItems = order.orderItems.OrderBy(p => p.lineNumber);

                    //loop through items to secure the inventory
                    int orderedLineNumbers = 1;
                    foreach (var itm in sortedOrderItems.ToArray())
                    {
                        var paramList = new OracleDynamicParameters();
                        paramList.Add("lv_co_odno", order.orderHeader.sorOrderNumber.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_cod_line", orderedLineNumbers, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_item", itm.sku.ToString(), dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_cod_qty", itm.quantity, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_total_allocated", itm.quantity, dbType: OracleMappingType.Int16, direction: ParameterDirection.Input);
                        paramList.Add("lv_unit_price", itm.priceInfo.unit_price, dbType: OracleMappingType.Decimal, direction: ParameterDirection.Input);
                        paramList.Add("lv_co_status", "W", dbType: OracleMappingType.Char, direction: ParameterDirection.Input);
                        paramList.Add("lv_em_no", "LCOWOO", dbType: OracleMappingType.Varchar2, direction: ParameterDirection.Input);
                        paramList.Add("lv_rows_affected", dbType: OracleMappingType.Int32, direction: ParameterDirection.Output);

                        storeProc = "LCBO.RESERVE_LCO_ORDER_DETAILS";

                        conn.Execute(storeProc, param: paramList, commandType: CommandType.StoredProcedure);

                        var rowsaffected = Decimal.ToInt32(paramList.Get<Decimal>("lv_rows_affected"));

                        if (rowsaffected == 1)
                        {
                            orderedLineNumbers = orderedLineNumbers + 1;
                        }

                        //Sanity check that number of items have been inserted into ON_CODTL Table
                        if (orderedLineNumbers != order.orderItems.Count())
                        {
                            isSuccess = false;
                        }
                    }
                    tran.Commit();

                    //build orderHeader invoice totals for W status, to return back to WCS for WOOCommerce estimate use.
                    if (order.orderHeader.invoiceInfo == null)
                    {
                        order.orderHeader.invoiceInfo = new InvoiceInfo();
                    }

                    order.orderHeader.invoiceInfo.InvoiceTotalAmount = order.orderItems.Sum(p => p.priceInfo.selling_price * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceDiscountAmount = order.orderItems.Sum(p => p.priceInfo.discount * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceBottleDepositAmount = order.orderItems.Sum(p => p.priceInfo.bottle_deposit * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceHstAmount = order.orderItems.Sum(p => p.priceInfo.hst_tax * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceLicMuAmount = order.orderItems.Sum(p => p.priceInfo.liquor_markup * p.quantity);
                    order.orderHeader.invoiceInfo.InvoiceLevyAmount = 0m;
                    order.orderHeader.invoiceInfo.InvoiceRetailAmount = order.orderItems.Sum(p => p.priceInfo.retail_price * p.quantity);

                    if (order.orderHeader.invoiceInfo.DeliveryCharge == null)
                    {
                        order.orderHeader.invoiceInfo.DeliveryCharge = new DeliveryCharge();
                        order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryBaseAmount = 0m;
                        order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryHstAmount = 0m;

                        //TODO: When we implement the proper delivery charge structure this logic should be removed.
                        if (order.orderHeader.deliveryType == DeliveryType.SHIP_TO_ADDR_ON_FILE.ToString())
                        {
                            order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryChargeType = DeliveryChargeType.CalculateDelivery.ToString();
                        }
                        else
                        {
                            order.orderHeader.invoiceInfo.DeliveryCharge.DeliveryChargeType = DeliveryChargeType.PickUp.ToString();
                            if (order.orderHeader.orderCommentsInfo == null)
                            {
                                order.orderHeader.orderCommentsInfo = new OrderCommentsInfo()
                                {
                                    BolComment = "PICK UP ORDER"
                                };
                            }
                        }
                    }

                    isSuccess = true;
                }
                catch(OracleException ex)
                {
                    var sErrorList = new List<ErrorDetail>();

                    if (ex.Number == 1422)
                    {
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = order.orderHeader.lcoOrderNumber + " already exist!",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = order.orderHeader.lcoOrderNumber.ToString()
                            }
                        });
                    }
                    else
                    {
                        sErrorList.Add(new ErrorDetail()
                        {
                            errorType = "INVALID_ORDER",
                            errorMessage = "Unhandling exception occurred.",
                            errorData = new ErrorDetail.DataValue()
                            {
                                key = ErrorType.OTHER.ToString(),
                                value = ex.Message.ToString()
                            }
                        });
                    }
                    order.errorInfo = sErrorList.ToArray();
                }
                finally
                {
                    if (!isSuccess) tran.Rollback();
                }

               // if (!isSuccess) return null;
                return Task.FromResult(order);
            }
        }

        // Need to save Jason string
        // Need to insert O status to orderstatus history
        // Silver 2.0, change Order status to be O:Open when OMS send order request.
    public Task<Order> CreateOrder(Order order, IEnumerable<ProductInventory> productList)
        {
            return ServiceHelper.CreateOrder(order, productList, _mapper, Connection);
        }

        public async Task<Order> GetOrderByIdAsync(string orderID)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                string OrderInvType = "E"; //use 'A' if order is already invoiced

                //if orderID is lcoOrderNumber use GET_ORDER_INFO_BY_REF instead
                string storeProc = "LCBO.GET_ORDER_INFO";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("lv_co_inv_type", OrderInvType);
                param.Add("lv_order", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string sQuery = storeProc;

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    //  var results = await conn.QueryAsync<OrderHeaderEntity>(sQuery, param: param, commandType: CommandType.StoredProcedure);
                    var results = await conn.QueryAsync<OrderHeaderEntity, RouteInfoEntity, OrderShipToInfoEntity,
                        OrderCommentsInfoEntity, PackingInvoiceCommentEntity, InvoiceInfoEntity, DeliveryChargeEntity,
                        OrderHeaderEntity>(sQuery, map: (header, route, ship, comment, packing, inv, delivery) =>
                         {
                             header.routeInfo = route;
                             header.shipToInfo = ship;
                             if (comment != null)
                             {
                                 header.orderCommentsInfo = comment;
                             }

                             if (packing != null)
                             {
                                 header.orderCommentsInfo.PackingInvoiceComment = packing;
                             }

                             header.invoiceInfo = inv;
                             header.invoiceInfo.DeliveryCharge = delivery;
                             return header;
                         }
                            , splitOn: "routeCode,shipToName,bolcomment,textLine1,invoiceTotalAmount,deliveryChargeType"
                            , param: param, commandType: CommandType.StoredProcedure);

                    //Need to map back to Product

                    //var dt = new DateTime();
                    //DateTime.TryParse("2019-06-01 18:30:45", out dt);
                    //var fo = dt.ToString("yyyy-MM-dd hh24:mm:ss A Z");
                    ////dt.t
                    ///

                    var order = new OrderEntity();

                    var orderHeaderInfo = results.SingleOrDefault();

                    order.orderHeader = orderHeaderInfo;

                    var orderItems = await GetOrderItemDetails(orderID);

                    order.orderItems = orderItems.ToArray();

                    return _mapper.Map<Order>(order);

                };

            }

            return null; //otherwise return null
        }

        public async Task<IEnumerable<OrderItemsEntity>> GetOrderItemDetails(string orderID)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                string OrderInvType = "E"; //use 'A' if order is already invoiced

                //if orderID is lcoOrderNumber use GET_ORDER_INFO_BY_REF instead
                string storeProc = "LCBO.GET_ORDER_DETAIL_INFO";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("lv_co_inv_type", OrderInvType);
                param.Add("lv_orderdetails", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                string sQuery = storeProc;

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderItemsEntity, OrderItemPriceInfoEntity, OrderItemsEntity>(sQuery, map: (item, price) =>
                       {
                           item.priceinfo = price;
                           return item;
                       }, splitOn: "sellingPrice", param: param, commandType: CommandType.StoredProcedure);


                    return results.ToArray();
                }
            }
            return null;
        }
  
        //Get order history status in sequence
        public async Task<IEnumerable<OrderStatusDetailEntity>> GetOrderPublishedStatus(string orderID)
        {
            using (IDbConnection conn = Connection)
            {
                string RocOrderID = orderID;
                string storeProc = "LCBO.GET_ORDER_PUBLISHED_STATUS";

                var param = new OracleDynamicParameters();
                param.Add("lv_sorOrderNumber", RocOrderID);
                param.Add("ref_orderPublishedStatus", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderStatusDetailEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);

                    return results.ToArray();
                }
            }
            return null;

        }

        //Get all possible order status life cycle path
        public async Task<IEnumerable<OrderStatusDetailEntity>> GetSystemOrderStatusCycle()
        {
            using (IDbConnection conn = Connection)
            {
                string storeProc = "LCBO.GET_ORDER_STATUS_CYCLE";

                var param = new OracleDynamicParameters();
                param.Add("lv_orderStatusCycle", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                if (conn.State == ConnectionState.Open)
                {
                    var results = await conn.QueryAsync<OrderStatusDetailEntity>(storeProc, param: param, commandType: CommandType.StoredProcedure);
                    return results.ToArray();
                }
            }
            return null;

        }
    }
}
 