using RoconLibrary.Models;

namespace RoconLibrary.Services
{
    public interface ILcoErrorLogService
    {
        void insertToTable(LcoErrorLog lcoErrorLog);
    }
}