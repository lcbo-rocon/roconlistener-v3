﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections;
using System.Data;
using LCODeliverySchedule.Models;
using RoconLibrary.Utility;

namespace LCODeliverySchedule.Services
{
    public static class DeliveryScheduleHelper
    {
        public static OracleCommand GetDBCommand(string query, OracleConnection connection)
        {
            ConnectionUtil.openConnection(connection);
            OracleCommand DBCommand = new OracleCommand(query, connection);
            return DBCommand;
        }

        public static DataTable GetDeliveryConfig(string DeliveryCode, OracleConnection connection)
        {
            DataTable dtDeliveryConfig = new DataTable();
            var query = "SELECT * FROM LCBO.LCO_DELIVERY_CONFIG ORDER BY DELIVERY_CODE ASC";
            if(!String.IsNullOrEmpty(DeliveryCode)) {
                query = "SELECT * FROM LCBO.LCO_DELIVERY_CONFIG WHERE DELIVERY_CODE = :DeliveryCode";
            }
            OracleCommand DBCommand = GetDBCommand(query, connection);
            DBCommand.Parameters.Clear();
            DBCommand.Parameters.Add(":DeliveryCode", DeliveryCode);

            dtDeliveryConfig.Load(DBCommand.ExecuteReader());
            connection.Close();
            return dtDeliveryConfig;
        }

        public static DataTable GetHolidays(OracleConnection connection)
        {
            DataTable dtHolidays = new DataTable();
            string query = "SELECT * FROM LCBO.HOLIDAY_EXCLUSION ORDER BY HOLIDAY_DT ASC";
            OracleCommand DBCommand = GetDBCommand(query, connection);
            dtHolidays.Load(DBCommand.ExecuteReader());
            connection.Close();

            return dtHolidays;
        }

        public static int LoadDeliverySchedulePreview(ArrayList lstDeliverySchedule, OracleConnection connection)
        {
            int rowsaffected = 0;
            string query = "INSERT INTO LCBO.LCO_DELIVERY_SCHEDULE_PREVIEW " +
                "(YEAR_WEEK,DELIVERY_CODE,CUTOFF_DT,DELIVERY_DT,IS_HOLIDAY, LAST_UPDATE_BY) " +
                "VALUES " +
                "(:YEAR_WEEK,:DELIVERY_CODE,:CUTOFF_DT,:DELIVERY_DT,:IS_HOLIDAY, :LAST_UPDATE_BY) ";

            OracleCommand DBCommand = GetDBCommand(query, connection);
            foreach (DeliverySchedule ds in lstDeliverySchedule)
            {
                DBCommand.Parameters.Clear();

                DBCommand.Parameters.Add(":YEAR_WEEK", ds.YearWeek);
                DBCommand.Parameters.Add(":DELIVERY_CODE", ds.DeliveryCode);
                DBCommand.Parameters.Add(":CUTOFF_DT", ds.CutOffDateTime);
                DBCommand.Parameters.Add(":DELIVERY_DT", ds.DeliveryDate);
                DBCommand.Parameters.Add(":ISHOLIDAY", ds.isHolidaySchedule == true? 1:0);
                DBCommand.Parameters.Add(":LAST_UPDATE_BY", "ITB2B");

                rowsaffected = rowsaffected + DBCommand.ExecuteNonQuery();
            }
            connection.Close();

            return rowsaffected;
        }

        public static int DeleteLoadDeliverySchedulePreview(OracleConnection connection)
        {
            string query = "Delete from lcbo.LCO_DELIVERY_SCHEDULE_PREVIEW";
            OracleCommand DBCommand = GetDBCommand(query, connection);
            int rowsaffected = DBCommand.ExecuteNonQuery();
            return rowsaffected;
        }

        public static DataTable GetDeliverySchedule(string DeliveryCode, OracleConnection connection)
        {
            DataTable dtDeliverySchedules = new DataTable();
            string query = "LCBO.GET_DELIVERY_DT_BY_ROUTE";
            OracleCommand DBCommand = GetDBCommand(query, connection);
            DBCommand.CommandType = CommandType.StoredProcedure;

            DBCommand.Parameters.Clear();
            DBCommand.Parameters.Add("lv_order_date", OracleDbType.Varchar2).Value = DateTime.Today.ToString("yyyy-MM-dd HH:mm");
            DBCommand.Parameters.Add("lv_number_of_available", OracleDbType.Int32).Value = 3;
            DBCommand.Parameters.Add("lv_route_code", OracleDbType.Varchar2).Value = DeliveryCode;
            DBCommand.Parameters.Add("cur_employees", OracleDbType.RefCursor).Direction = ParameterDirection.Output;
            DBCommand.Connection.Open();

            dtDeliverySchedules.Load(DBCommand.ExecuteReader());

            DBCommand.Connection.Close();

            return dtDeliverySchedules;
        }


    }
}
