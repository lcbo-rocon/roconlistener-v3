﻿/* Logic to calculate the available delivery date on week or Bi-weekly */
/* Need to hook up with Database to get list of Holiday dates */
/* Can use to build interface for administrator to manage the cutoff datetime */
/* ASSUMPTIONS:
 * When Delivery Date lands on a holiday (or excluded day), 
 * it will assume the cutoff datetime is a day earlier and the will choose the earliest
 * next day for delivery. 
 * For example, if delivery date lands on 2019-04-19 (Good Friday) it will calculate and move the 
 * Delivery date to 2019-04-23 because 2019-04-22 (Easter Monday) is another holiday.
 *
 * UPDATE: Included logic to Load LCBO.LCO_DELIVERY_SCHEDULE based on delivery config
 *
 *
 *
 */

using System;
using System.Collections;
using System.Data;
using System.Globalization;
using LCODeliverySchedule.Services;
using LCODeliverySchedule.Models;
using Oracle.ManagedDataAccess.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using System.IO;
using RoconLibrary.Utility;

namespace LCODeliverySchedule
{
    class Program
    {
        static private Hashtable HolidayTbl = new Hashtable();
        static private Hashtable prevCutoffDate = new Hashtable();

        static void Main(string[] args)
        {
            IServiceCollection services = new ServiceCollection();
            Startup startup = new Startup();
            startup.ConfigureServices(services);
            IServiceProvider serviceProvider = services.BuildServiceProvider();

            var config = serviceProvider.GetService<IConfigurationRoot>();

            setupLogger(config);

            Log.Information($"Current dir: {Directory.GetCurrentDirectory()}");

            var connection = ConnectionUtil.getConnection(config);

            foreach (string arg in args)
            {
                Log.Information("Argument value is: " + arg);
            }
            Log.Information("\n");
            startProcess(args, connection, config);
            // Console.ReadLine();
        }

        static private void setupLogger(IConfigurationRoot config)
        {
            var loggingFilePath = config["Serilog:filepath"];
            var outputTemplate = config["Serilog:outputTemplate"];
            var rollingInterval = config["Serilog:rollingInterval"];
            var fileSizeLimitBytes = Int32.Parse(config["Serilog:fileSizeLimitBytes"]);
            var minimumLevel = config["Serilog:minimumLevel"];

            var level = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), minimumLevel);

            Log.Logger = new LoggerConfiguration()
                            .MinimumLevel.Is(level)
                            .WriteTo.Console()
                            .WriteTo.File(loggingFilePath,
                                          outputTemplate: outputTemplate,
                                          fileSizeLimitBytes: fileSizeLimitBytes,
                                          rollingInterval: (RollingInterval)Enum.Parse(typeof(RollingInterval), rollingInterval),
                                          restrictedToMinimumLevel: level)
                            .CreateLogger();
        }

        private static void startProcess(string[] args, OracleConnection connection, IConfigurationRoot config)
        {
            Log.Information("=======================================");
            Log.Information("Program usage example (arguments are optional):");
            Log.Information("      dotnet run initialDate=\"2020-08-01\" year=2020 deliveryCode=\"BR06\"");
            Log.Information("\n");

            var defaultYear = Int32.Parse(config["year"]);
            var defaultInitialDate = config["initialDate"];

            Log.Information("Default values if not argument(s) are passed:");
            Log.Information($"      initialDate: \"{defaultInitialDate}\"");
            Log.Information($"      year: {defaultYear}");
            Log.Information("      deliveryCode: ''");
            Log.Information("Default values can be change in appsettings configuration file");
            Log.Information("=======================================");
            Log.Information("\n");

            CommandLineArgs.I.parseArgs(args, $"initialDate={defaultInitialDate};year={defaultYear}");
            var initialDate = CommandLineArgs.I.argAsString("initialDate");
            var year = CommandLineArgs.I.argAsLong("year");
            var deliveryCode = CommandLineArgs.I.argAsString("deliveryCode");

            Log.Information("Arg [initialDate]: '{0}' ", initialDate);
            Log.Information("Arg [year]: '{0}' ", year);
            Log.Information("Arg [deliveryCode]: '{0}' ", deliveryCode);

            DataTable deliveryConfigTable = DeliveryScheduleHelper.GetDeliveryConfig(deliveryCode, connection);

            //This code for normal delivery dates with one date
            DateTime dtInitialDate;
            DateTime.TryParse(initialDate, out dtInitialDate);

            var dtData = new ScheduleDates(dtInitialDate, connection);
            HolidayTbl = dtData.ExcludedDates;
            prevCutoffDate = dtData.PrevCutoffDayOfWeeks;

            ArrayList lstDeliverySchedule = new ArrayList();

            while (dtInitialDate.Year <= year)
            {
                Log.Information($"Year is: {dtInitialDate.Year}");
                foreach (DataRow dr in deliveryConfigTable.Rows)
                {
                    DayOfWeek CutoffDOW;
                    DayOfWeek DeliveryDOW;
                    DayOfWeek HolCutoffDOW;
                    DayOfWeek HolDeliveryDOW;

                    DayOfWeek.TryParse(dr["CUTOFF_DAY_OF_WEEK"].ToString(), out CutoffDOW);
                    DayOfWeek.TryParse(dr["DELIVERY_DAY_OF_WEEK"].ToString(), out DeliveryDOW);
                    DayOfWeek.TryParse(dr["HOLIDAY_CUTOFF_DAY_OF_WEEK"].ToString(), out HolCutoffDOW);
                    DayOfWeek.TryParse(dr["HOLIDAY_DELIVERY_DAY_OF_WEEK"].ToString(), out HolDeliveryDOW);

                    DeliverySchedule delsched = CalcDeliverySchedule(
                        dr["DELIVERY_CODE"].ToString(), dtInitialDate, CutoffDOW, DeliveryDOW, HolCutoffDOW, HolDeliveryDOW);

                    Log.Information(delsched.ToString());

                    lstDeliverySchedule.Add(delsched);
                }
                dtInitialDate = dtInitialDate.AddDays(7);
            }

            //Delete then Load to DB
            int recordsDeleted = DeliveryScheduleHelper.DeleteLoadDeliverySchedulePreview(connection);
            int recordsAdded = DeliveryScheduleHelper.LoadDeliverySchedulePreview(lstDeliverySchedule, connection);

            Log.Information("There were records " + recordsDeleted + " deleted.");
            Log.Information("There were records " + recordsAdded + " loaded.");
        }

        static private DeliverySchedule CalcDeliverySchedule(string route, DateTime dtStartDate, DayOfWeek cutoffDOW, DayOfWeek deliveryDOW
            , DayOfWeek HolCutoff, DayOfWeek HolDelivery)
        {
            DeliverySchedule schDelivery = new DeliverySchedule();
            DateTime dtStartOfWeek = dtStartDate;
            DateTime dtCutoffDateTime;
            DateTime dtDeliveryDateTime;
            bool isHolidaySchedule = false; //flag set to true if cutoff/delivery is adjusted from normal cutoff/delivery

            //Find out the Sunday for the date provided
            while (dtStartOfWeek.DayOfWeek != DayOfWeek.Sunday)
            {
                dtStartOfWeek = dtStartOfWeek.AddDays(-1);
            }

            //Calculate the cutoff datetime
            dtCutoffDateTime = dtStartOfWeek;
            while (dtCutoffDateTime.DayOfWeek != cutoffDOW)
            {
                dtCutoffDateTime = dtCutoffDateTime.AddDays(1);
            }

            //Check if cutoff time is a holiday
            if (isHoliday(dtCutoffDateTime.ToString("yyyy-MM-dd")))
            {
                isHolidaySchedule = true;

                //go back to first non-holiday date
                while (dtCutoffDateTime.DayOfWeek != HolCutoff)
                {
                    dtCutoffDateTime = dtCutoffDateTime.AddDays(-1);
                }
                while (isHoliday(dtCutoffDateTime.ToString("yyyy-MM-dd")))
                {
                    dtCutoffDateTime = dtCutoffDateTime.AddDays(-1);
                }
            }

            //calculate the delivery datetime
            dtDeliveryDateTime = dtCutoffDateTime;
            while (dtDeliveryDateTime.DayOfWeek != deliveryDOW)
            {
                dtDeliveryDateTime = dtDeliveryDateTime.AddDays(1);
            }

            //Check if delivery date is holiday and will need to shift
            if (isHoliday(dtDeliveryDateTime.ToString("yyyy-MM-dd")))
            {
                isHolidaySchedule = true;

                while (dtCutoffDateTime.DayOfWeek != HolCutoff)
                {
                    dtCutoffDateTime = dtCutoffDateTime.AddDays(-1);
                    if (isHoliday(dtCutoffDateTime.ToString("yyyy-MM-dd")))
                    {
                        dtCutoffDateTime = dtCutoffDateTime.AddDays(-1);
                    }
                }

                //get new delivery date
                while (dtDeliveryDateTime.DayOfWeek != HolDelivery)
                {
                    dtDeliveryDateTime = dtDeliveryDateTime.AddDays(1);
                }

                //shift delivery date one more time if it a holdiay
                while (isHoliday(dtDeliveryDateTime.ToString("yyyy-MM-dd")))
                {
                    dtDeliveryDateTime = dtDeliveryDateTime.AddDays(1);
                }

            }

            //Exception occurs when delivery date is on Sunday for some reason it picks up last sunday
            //which results to cutoff datetime and delivery date being the same date in this case we need
            //to shift the delivery date by a week.
            if (dtDeliveryDateTime.ToString("yyyy-MM-dd") == dtCutoffDateTime.ToString("yyyy-MM-dd"))
            {
                dtDeliveryDateTime = dtDeliveryDateTime.AddDays(7);
                while (isHoliday(dtDeliveryDateTime.ToString("yyyy-MM-dd")))
                {
                    dtDeliveryDateTime = dtDeliveryDateTime.AddDays(1);
                }
            }

            //set the cutoff time
            TimeSpan ts = new TimeSpan(10, 00, 0);

            dtCutoffDateTime = dtCutoffDateTime.Date + ts;

            schDelivery.YearWeek = dtStartOfWeek.ToString("yyyy") + GetWeekNumber(dtStartOfWeek).ToString("00");
            schDelivery.DeliveryCode = route;
            schDelivery.CutOffDOW = cutoffDOW;
            schDelivery.DeliveryDOW = deliveryDOW;
            schDelivery.CutOffDateTime = dtCutoffDateTime;
            schDelivery.DeliveryDate = dtDeliveryDateTime;
            schDelivery.isHolidaySchedule = isHolidaySchedule;

            return schDelivery;
        }

        static private int GetWeekNumber(DateTime dt)
        {
            CultureInfo cul = CultureInfo.CurrentCulture;
            int weekNum = cul.Calendar.GetWeekOfYear(
                dt,
                CalendarWeekRule.FirstFourDayWeek,
                DayOfWeek.Monday);

            return weekNum;
        }

        static private DateTime calcNextDeliveryDate(DateTime dtFromDate,
                                            DayOfWeek cutoff,
                                            DayOfWeek delivery,
                                            DayOfWeek holcutoff,
                                            DayOfWeek holdelivery)
        {
            DateTime dtNextDeliveryDate = new DateTime();
            DateTime dtCutOffDate = dtFromDate;
            int leadDays = (int)delivery - (int)cutoff;

            if (leadDays < 0) leadDays = leadDays + 7;

            //check if cutoff date is on a holiday find the next non-holiday day to be the order date
            while (isHoliday(dtFromDate.ToString("yyyy-MM-dd")))
            {
                dtFromDate = dtFromDate.AddDays(1);
            }

            while (dtCutOffDate.DayOfWeek != cutoff)
            {
                dtCutOffDate = dtCutOffDate.AddDays(1);
            }

            dtNextDeliveryDate = dtFromDate;

            dtNextDeliveryDate = dtNextDeliveryDate.AddDays(leadDays);

            while (dtNextDeliveryDate.DayOfWeek != delivery)
                dtNextDeliveryDate = dtNextDeliveryDate.AddDays(1);

            //If the delivery date is a holiday - expectation is cutoff time is earlier
            if (isHoliday(dtNextDeliveryDate.ToString("yyyy-MM-dd")))
            {
                //or explore how to calculate the previous cutoff day of week because it is not always 1
                //TODO
                dtCutOffDate = dtFromDate;
                while (dtCutOffDate.DayOfWeek != holcutoff)
                {
                    dtCutOffDate = dtCutOffDate.AddDays(1);
                }

                if (dtFromDate > dtCutOffDate)
                {
                    Console.Write("Holiday Cutoff date not met...moving date from " + dtNextDeliveryDate.ToString("yyyy-MM-dd") + " to ");
                    //means cut off was not met and need to recalculate
                    dtNextDeliveryDate = calcNextDeliveryDate(dtFromDate.AddDays(1), cutoff, delivery, holcutoff, holdelivery);
                    Log.Information(dtNextDeliveryDate.ToString("yyyy-MM-dd"));

                }
            }

            while (isHoliday(dtNextDeliveryDate.ToString("yyyy-MM-dd")))
                dtNextDeliveryDate = dtNextDeliveryDate.AddDays(1);

            return dtNextDeliveryDate;
        }

        static private bool isHoliday(string strDate)
        {
            return HolidayTbl.ContainsKey(strDate);

        }
    }
}
