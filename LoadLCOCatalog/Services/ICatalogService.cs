﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using RoconLibrary.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoadLCOCatalog.Services
{
    public interface ICatalogService
    {
        void processStart(OracleConnection connection);
    }
}
